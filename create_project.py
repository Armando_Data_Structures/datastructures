"""Usage: create_project.py <project_name> <package>...

Arguments:
  NAME  name of the project

"""

from docopt import docopt
from subprocess import Popen, PIPE
 
def create_project_dir(args):
   print("Creating Dir...")
   Popen(['mkdir', args["<project_name>"]])
   #print (args["<package>"])
   for v in args["<package>"]:
      list = v.split(".")
      #print(list)
      temp = args["<project_name>"] + "/"
      for i in list:
         temp = temp + i + "/" 
         out = Popen(['mkdir', temp], stdout=PIPE, stderr=PIPE)
   #p, e, _ = out.communicate()

def process_opts(args):
   create_project_dir(args)

if __name__ == '__main__':
   arguments = docopt(__doc__)
   process_opts(arguments)
