package com.bag.interfaces;

public interface BagInterface<T>
{

   public boolean add(T newEntry);
   public boolean isEmpty();
   public boolean isFull();
   public T[] toArray(); 
   public int getCurrentSize();
   public T remove();
   public boolean remove(T anEntry);
   public void clear();
   public int getFrequencyOf(T anEntry);
   public boolean contains(T anEntry);
}
