package com.bag.implementation;

import java.util.Arrays;

public class ResizableArrayBag<T> extends ArrayBag<T>
{
   public ResizableArrayBag()
   {
    super();  
   }
   
   public ResizableArrayBag(int bagSize)
   {
      super(bagSize);
   }

   private void ensureCapacity()
   {
      if(numberOfEntries == bag.length)
      {
         System.out.println("Current Bag's Size: "+bag.length);
         bag = Arrays.copyOf(bag, bag.length * 2);
         System.out.println("Duplicating bag size: "+bag.length);
      }
   }

   public boolean add(T newEntry )
   {
      ensureCapacity();
      bag[numberOfEntries] = newEntry;
      numberOfEntries++;
      return true;  
   }

   public boolean isFull()
   {
      return false;
   }

} 
