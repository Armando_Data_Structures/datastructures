package com.bag.implementation;

import com.bag.interfaces.BagInterface;
import java.util.Arrays;

public class ArrayBag<T> implements BagInterface<T>
{
   private final static int DEFAULT_INITIAL_CAPACITY = 20;
   private int BAG_SIZE;
   protected int numberOfEntries;
   protected T[] bag;

   public ArrayBag()
   {
      this(DEFAULT_INITIAL_CAPACITY);
   }

   @SuppressWarnings("unchecked")
   public ArrayBag(int bagSize)
   {
      bag =(T[]) new Object[bagSize];
      numberOfEntries = 0;
   }

   private int getIndexOf(T anEntry)
   {
      boolean found = false;
      int where = -1;
      for(int index = 0; index < numberOfEntries && !found; index++)
      {
         if(anEntry.equals(bag[index]))
         {
            found = true;
            where = index;
         }
      }
      return where;

      /*int index = -1;
      while(index < numberOfEntries - 1)
      {
         index++;
         if(anEntry.equals(bag[index]))
         {
            return index;
         }
      }
      return -1;*/
   }

   private T removeAnEntry(int index) 
   {
      T result = null;
      if(!isEmpty() && index >=0)
      {
         result = bag[index];
         numberOfEntries--;
         bag[index] = bag[numberOfEntries];
         bag[numberOfEntries] = null;
         return result;
      }

      return result;
   }

   @Override
   public boolean add(T newEntry)
   {
      if(!isFull())
      {
         bag[numberOfEntries] = newEntry;
         numberOfEntries++;
         return true;
      }
      return false;
   }

   @Override
   public boolean isEmpty()
   {
      return numberOfEntries == 0;
   }

   @Override
   public boolean isFull()
   {
      return numberOfEntries == bag.length;
   }
   
   @Override
   public T[] toArray()
   {
     @SuppressWarnings("unchecked")
      T[] tempArray = (T[])new Object[numberOfEntries];
      tempArray =  Arrays.copyOf(bag, numberOfEntries);
      return tempArray;
   }

   @Override
   public int getCurrentSize()
   {
      return numberOfEntries;
   }

   @Override
   public T remove()
   {
      return  removeAnEntry(numberOfEntries - 1);
   } 
  
   @Override
   public boolean remove(T anEntry)
   {
      return removeAnEntry(getIndexOf(anEntry)) != null; 
   }

   @Override
   public void clear()
   {
      while(remove() != null);
      numberOfEntries = 0;
   }

   @Override
   public int getFrequencyOf(T anEntry)
   {
      int count = 0;
      for(int index = 0; index < numberOfEntries; index++)
      {
         if(anEntry.equals(bag[index]))
         {
            count++;
         }
      }
      return count;
   }

   @Override
   public boolean contains(T anEntry)
   {
      return getIndexOf(anEntry) > -1;
   }
}
