package com.bag.implementation.linkedimplementation;

import com.bag.interfaces.BagInterface;

public class LinkedBag<T> implements BagInterface<T>
{
   private Node firstNode;
   private int numberOfEntries;

   public LinkedBag()
   {
      firstNode = null;
      numberOfEntries = 0;
   }

   @Override
   public boolean add(T newEntry)
   {
      Node node = new Node(newEntry);
      node.setNext(firstNode);
      firstNode = node;
     
      /*if(numberOfEntries == 0)
      {
         firstNode = new Node<>(newEntry);
      }else
      {
         
         firstNode = new Node<>(newEntry, firstNode);
      }*/
      numberOfEntries++;
      return true;
   }

   @Override
   public T remove()
   {
      T result = null;
      if(firstNode != null)
      {
         result = firstNode.getData();
         firstNode = firstNode.getNext();
         numberOfEntries--;
      }
      /*Node tmpNode = firstNode;
      if(!isEmpty())
      {
         firstNode.setNext(firstNode.getNext());
         numberOfEntries--;
      }*/
      return result;
   }

   @Override
   public boolean remove(T anEntry)
   {
    Node currentNode = firstNode;
    boolean found = false;
    while(!found && currentNode != null)
    {
       if(anEntry.equals(currentNode.getData()))
       {
          currentNode.setData(firstNode.getData());
          remove();
          found = true;
       }
       currentNode = currentNode.getNext();
    } 
      return found;
   }

   @Override
   public boolean isEmpty()
   {
      return numberOfEntries == 0;
   }

   @Override
   public boolean isFull()
   {
      return false;
   }

   @Override
   public int getCurrentSize()
   {
      return numberOfEntries;
   }

   @Override
   public int getFrequencyOf(T anEntry)
   {
      Node tmp = firstNode;
      int count =0;
      while(tmp != null)
      {
        if(anEntry.equals(tmp.getData()))
           count++;
        tmp = tmp.getNext();
      }

      return count;
   }

   @Override
   public boolean contains(T anEntry)
   {
      Node tmp = firstNode;
      boolean find = false;

      while(!find && tmp != null)
      {
         if(anEntry.equals(tmp.getData()))
         {
            find = true;
         }
         else
         {
            tmp = tmp.getNext();
         }
      }
      return find;
   }

   @Override
   public void clear()
   {
   
   }

   
   @Override
   public T[] toArray()
   {
      @SuppressWarnings("unchecked")
      T[] array = (T[]) new Object[numberOfEntries];
      Node tmp = firstNode;
      for(int index=0; index<numberOfEntries; index++)
      {
         array[index] = tmp.getData();
         tmp = tmp.getNext();
      }
      return array;
   }
  
 private class Node
   {
      private T data;
      private Node next;

      public Node(T data)
      {
         this(data, null);
      }

      public Node(T data, Node nextNode)
      {
         this.data = data;
         next = nextNode;
      }

      public T getData()
      {
         return data;
      }

      public Node getNext()
      {
         return next;
      }

      public void setData(T data)
      {
         this.data = data;
      }

      public void setNext(Node next)
      {
         this.next = next;
      }

      @Override
      public boolean equals(Object obj)
      {
        return this.data.equals(((Node)obj).getData());
      }
   }
}
