package com.bag.test;

import com.bag.interfaces.BagInterface;
import com.bag.implementation.ArrayBag;
import com.bag.implementation.ResizableArrayBag;

public class Test1
{
   public static BagInterface<String> bag = new ArrayBag<>();
   public static BagInterface<String> ResizableBag = new ResizableArrayBag<>();

   public static void main(String argv[])
   {
      for(int index =0; index < 80; index++)   
      {
         ResizableBag.add(""+index);
      }
      showArray(ResizableBag.toArray());

      //Test method add of Bag
      /*for(int i =0; i<50; i++)
      {
        System.out.println("Data " + i + " Added? :"+bag.add(new String(""+i)));
      }*/

      //Test getFrequencyOf method
      bag.add("1");
      bag.add("1");
      bag.add("1");
      bag.add("2");
      bag.add("2");
      System.out.println("Frequency of 1: "+ bag.getFrequencyOf("1"));
      System.out.println("Frequency of 2: "+ bag.getFrequencyOf("2"));

      //Test contains method
      System.out.println("Bag contains the number 3?: "+bag.contains("3"));
      bag.add("3");
      System.out.println("Added value 3...");
      System.out.println("Bag contains the value 3? "+ bag.contains("3"));
      removeMethodTests(); 
   }
   //Remove method tests
   public static void removeMethodTests()
   {
      /*1.-Get Frequency of an entry.
        2.-Remove that entry once.
        3.-Check if remove was performed well.
        3.-Get Frequency of the entry again.
        4.-Compare previous frequency and current.
      */ 
      String entryToRemove = "3";
      int prevF = bag.getFrequencyOf(entryToRemove);
      System.out.println("Removing entry: "+entryToRemove); 
      boolean result = bag.remove(entryToRemove); 
      System.out.println("Was Removed : "+ result);
      assert result == true;
      int currF = bag.getFrequencyOf(entryToRemove);
      System.out.println("Previous Frequency : "+prevF+" Frequency after remove: "+currF);
      assert currF < prevF; 
   }

   public static void showArray(Object bag[])
   {
      //Object []tmpBag = bag.toArray();
      for(int index=0; index < bag.length; index++)
      {
         System.out.println(""+bag[index].toString());
      }
   }

}
