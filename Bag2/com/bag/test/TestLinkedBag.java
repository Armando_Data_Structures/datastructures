package com.bag.test;

import com.bag.implementation.linkedimplementation.*;
import com.bag.interfaces.*;

public class TestLinkedBag
{
   public static BagInterface<String> bag = new LinkedBag<>();

   public static void main(String args[])
   {
      bag.add("val1");
      System.out.println("Is Empty: "+bag.isEmpty());
      bag.add("val2");
      bag.add("val3");
      bag.add("val4");
      bag.add("val5");
      bag.add("val6");
      bag.add("val7");
      System.out.println("Number of Entries: "+bag.getCurrentSize());
      
      System.out.println("Frequency of val1:"
                         +bag.getFrequencyOf("val1"));
      System.out.println("Contains val1?: "+bag.contains("val1"));
      System.out.println("Contains val3?: "+bag.contains("val3"));
      System.out.println("Contains val10?: "+bag.contains("val10"));

      //Test Remove method of LinkedBag
      System.out.println("Bag before remove");
      showArray(bag.toArray());
      //bag.remove();
      System.out.println("Bag after remove val1: "+bag.remove("val1"));
      showArray(bag.toArray());
      
   }

   public static void showArray(Object[] array)
   {
      for(int index =0; index < array.length; index++)
      {
         System.out.println(""+array[index]);
      }
   }
}


