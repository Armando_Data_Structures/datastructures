JC = javac
#JFLAGS = -Xlint -d
JFLAGS =  -d
.SUFFIXES: .java .class
CLASS_DIR = ./classes
RUN = java -ea -classpath ./classes com.bag.test.Test1
all:
	$(JC) $(JFLAGS) $(CLASS_DIR) $(FILES)

FILES = com/bag/interfaces/BagInterface.java	\
	com/bag/implementation/ArrayBag.java	\
	com/bag/test/Test1.java
	#src/com/data_structures/test/Main.java	\

EXEC:
	$(RUN)
	
